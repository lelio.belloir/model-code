import numpy as np
from math import exp
from scipy.stats import beta   # Pour la beta distribution que j'ai pas encore ajoutée
import matplotlib.pyplot as plt


####  Forward v2.0

#### Initialisation ####

T_final = 100 # Number of time steps
a = 0.6      # probability of encountering another individual during a time step
Nclasses = 20 # Number of quality classes
Q_min = 0    # minimum quality
Q_max = Nclasses    # maximum quality


## Classes of Quality
Qs_classes = []
for k in range(Nclasses):
    Qs_classes.append((k+1/2)*(Q_max - Q_min)/Nclasses)  # k+1/2 pour que Qs_classes[k] contienne la valeur du milieu de la classe On pourra le changer si on veut avoir la moyenne de la distrib pour cette classe (mais ce n'est pas si utile vu que la distribution n'est vraie qu'a t=0)
Qp_classes = Qs_classes.copy()
Qp_classes.insert(0,0) # we add a first Qp = 0 if single


## Quality matrix
Q = []
for t in range(T_final+1):
    Q.append(Qp_classes)
Q = np.transpose(np.array(Q))


## Non Mated individuals
Distribution_Matrix = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1)) #contains porportions of individuals distributed among the differents states
for iQs in range(len(Qs_classes)): # At time t=0 every individuals are single (and their proportions follows the initial distribution of quality)
    Distribution_Matrix[0][iQs][0][0] = 1/len(Qs_classes) #uniform distribution
# Remark : At any time step, the coefs of this matrix are porportions of the initial population => at time 0, sum(Distribution_Matrix = 1) but for the following time steps the sum is less than 1 since mated individuals are removed from the population  
# Remark 2 : at any time step, np.sum(Distribution_Matrix[t] + Mate[t]) should be 1
# Remark 3 : at any time step this matrix should be symmetric with : Distribution_Matrix[t][Qs][Qp] = Distribution_Matrix[t][Qp][Qs]


## Mated individuals
Mate = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1)) # contains porportions of mated couples. Initially, no one is in Mate
# Remark : at any time step this matrix should be symmetric with : Mate[t][Qs][Qp] = Mate[t][Qp][Qs]


## probability for a courtship pair to mate
T = 20      # 
labda = 1   #                     ("lambda" is one of python's key words. Hence we use "labda")

def y(t_prime,T,labda) : # gamma function : mating probability for a couple at courtship time = t_prime 
    return 1/(1+exp(labda*(T-t_prime))) # 

P_mate = []
for t_prime in range(T_final):
    P_mate.append(y(t_prime,T,labda))


## Biais : initialization with a null matrix
Biais = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1)) #POUR L'INSTANT J'AI laissé Qp pour des bails de formats, faudra soit changer, soit faire des matrices avec le même biais pour tous les Qp


## Biased quality matrix
Q_prime = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1))
for t in range(T_final):
    for iQs in range(len(Qs_classes)):
        for iQp in range(len(Qp_classes)):
            Q_prime[t][iQs][iQp] += Q[iQp][t_prime] + Biais[t][iQs][iQp]


Proportion_Matrix = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1)) #



## Variables useful in output
P_courtship = [0]
P_mateA = [0]
P_mateB = [0 for k in range(T_final)]
Q_single_moy = [0]
Q_courtship_moy = [0]

New_Courtship = np.zeros((T_final+1,len(Qs_classes),len(Qs_classes),len(Qp_classes),T_final+1)) # temporary matrix containing the "proportion" of individuals that initiated a courtship with a new partner during the time step
Become_single = np.zeros((T_final+1,len(Qs_classes),len(Qs_classes),len(Qp_classes),T_final+1))                 # temporary matrix containing the "proportion" of individuals that became single during the time step
Become_Mated = np.zeros((T_final+1,len(Qs_classes),len(Qp_classes),T_final+1))                  # temporary matrix containing the "proportion" of individuals that mated during the time step
N = np.zeros((T_final,len(Qs_classes))) # "Proportion" of individuals of each quality
TRUC = [] #pour débugguer : contiendra la somme des individus

#### MAIN ####  

for t in range(T_final):
    print("t =",t)
    # Creation of temporary matrixes? that will be used to store values during computation !!! Si vous avez des noms plus jolis, hésitez pas !!!
    Remaining_Population = Distribution_Matrix[t].copy()                                   # temporary matrix containing the "proportion" of individuals that have yet to change their state during the time step. Remark : at any time during the execution, we should have : sum(Distribution_Matrix[t+1] + sum(Remaining_population)) = 1
    Suitable = np.zeros((len(Qs_classes),len(Qs_classes),len(Qp_classes),T_final+1))       # temporary matrix storing the states for which an individual would find another individual better than its current partner
    #New_Courtship = np.zeros((len(Qs_classes),len(Qs_classes),len(Qp_classes),T_final+1)) # temporary matrix containing the "proportion" of individuals that initiated a courtship with a new partner during the time step
    #Become_single = np.zeros((len(Qs_classes),len(Qs_classes),len(Qp_classes),T_final+1)) # temporary matrix containing the "proportion" of individuals that became single during the time step
    #Become_Mated = np.zeros((len(Qs_classes),len(Qp_classes),T_final+1))                  # temporary matrix containing the "proportion" of individuals that mated during the time step
    
    Mate[t+1] += Mate[t] # Mated individuals are still mated at the next time step
    
    for iQs in range(len(Qs_classes)):
        N[t][iQs] = np.sum(Distribution_Matrix[t][iQs])
        Proportion_Matrix[t][iQs] = Distribution_Matrix[t][iQs]/N[t][iQs]
                         
        for iQs_2 in range(len(Qs_classes)): #
            Suitable[iQs][iQs_2] = np.array(Q_prime[t][iQs]) < Qs_classes[iQs_2] # Suitable[iQs][iQs2][iQp][t_prime] is a boolean that answers the question "is Qs_2 better than the estimation by Qs of its Qp partner at courtship time = t_prime"
    Ntot = np.sum(N[t])

    
    if t == 0 : # first time step is different so that no error occur (else, when trying to select individuals in courtship with t_prime = [1:(t+1)], the output will have no dimension wich is a problem for computation)
        ## find a new courtship partner ##
        # since every individual is single, it automatically starts a new courtship provided it encounters a potential partner 
        for iQs in range(len(Qs_classes)):
            for iQs_2 in range(len(Qs_classes)): # 
                New_Courtship[t][iQs][iQs_2][0][0] = (a*(N[t][iQs_2]/Ntot)                 # Probability to encounter an individual of quality Qs_2
                                                    *Remaining_Population[iQs][0][0]) #
            
            for iQs_2 in range(len(Qs_classes)):
                Remaining_Population[iQs][0][0] -= New_Courtship[t][iQs][iQs_2][0][0]           #
                Distribution_Matrix[t+1][iQs][iQs_2+1][1] += New_Courtship[t][iQs][iQs_2][0][0] # Store the "proportion" of individuals that started a new courtship
            
            Distribution_Matrix[t+1][iQs][0][0] += Remaining_Population[iQs][0][0]              # the remaining single individuls (that didn't encounter anyone or were not accepted) will stay single until next time step 


    else :
        ## find a new courtship partner ##
        # The individual starts a new courtship if the encountered individual is better than its estimation of its current partner (Qs_2_prime > Qp_prime) and if the encountered individual finds it better than its current partner (Qs_prime > Qp_2_prime)
        for iQs in range(len(Qs_classes)):
            for iQs_2 in range(len(Qs_classes)): # 
                for iQp in range(len(Qp_classes)): 
                    if iQp == 0:
                        New_Courtship[t][iQs][iQs_2][0][0] += (a*(N[t][iQs_2]/Ntot)                                   # Probability to encounter an individual of quality Qs_2
                                                            *np.sum(Proportion_Matrix[t][iQs_2]*Suitable[iQs_2][iQs]) # Probability for Qs to be accepted by Qs_2
                                                            *Remaining_Population[iQs][0][0])                         # "Proportion" of single individuals of Qs quality
                    else :
                        New_Courtship[t][iQs][iQs_2][iQp][1:(t+1)] += (a*(N[t][iQs_2]/Ntot)                                                    # Probability to encounter an individual of quality Qs_2
                                                                  *np.sum(Proportion_Matrix[t][iQs_2]*Suitable[iQs_2][iQs])                    # Probability for Qs to be accepted by Qs_2
                                                                  *Remaining_Population[iQs][iQp][1:(t+1)]*Suitable[iQs][iQs_2][iQp][1:(t+1)]) # "Proportion" individuals of Qs quality thant would find a Qs2 partner better than their current partner (Qp)
        
            for iQs_2 in range(len(Qs_classes)):
                Remaining_Population[iQs,:,:] -= New_Courtship[t,iQs,iQs_2,:,:] 
                
                Distribution_Matrix[t+1,iQs,iQs_2+1,1] += np.sum(New_Courtship[t][iQs][iQs_2]) # Store the "proportion" of individuals that started a new courtship. Remark : when selecting the index of the partner (iQs_2), we add +1 since the firt index iQp=0 refers to single individuals (no partner)
                
        ## Partner finds a new courtship partner ##
        # the individual becomes single if it doesn't initiate a new courtship but its partner does
        for iQs in range(len(Qs_classes)):            
            for iQs_2 in range(len(Qs_classes)): 
                for iQp in range(len(Qs_classes)): # remark : here Qp cannot be 0 (no partner). Hence we use "len(Qs_classes)". in the lines below, we use iQp + 1 since the first index represents no partner (single individuals)
                    Become_single[t][iQs][iQs_2][iQp][1:(t+1)] += (Remaining_Population[iQs][iQp+1][1:(t+1)]   # Proportion of individuals that didn't find a better partner. remark : iQp+1 so as to avoid the case Qp=0 (no partner)
                                                              *np.nan_to_num((New_Courtship[t][iQp][iQs_2][iQs+1][1:(t+1)]/Distribution_Matrix[t][iQp][iQs+1][1:(t+1)]),copy=False)) # !!!!! ERROR !!! TRUE DIVIDE # Probability for a Qp partner to change for a better partner (= ("proportion" that changed) / (proportion of individual initially in this state))
        
        for iQs in range(len(Qs_classes)):            
            for iQs_2 in range(len(Qs_classes)): 
                Remaining_Population[iQs,:,1:(t+1)] -= Become_single[t,iQs,iQs_2,:,1:(t+1)] 
                
            Distribution_Matrix[t+1][iQs][0][0] += np.sum(Become_single[t][iQs]) # Store the "proportion" of newly single individuals 

            
        ## The courtship pair mate ##
        for iQs in range(len(Qs_classes)):
            for iQp in range(len(Qp_classes)): #
                Become_Mated[t][iQs][iQp][1:(t+1)] += P_mate[1:(t+1)]*Remaining_Population[iQs][iQp][1:(t+1)] # between time t' and t+1, the courtship partners can mate with a probability P_mate[t'] = y(t',T,lambda)
                
                Mate[t+1][iQs][iQp][1:t] += Become_Mated[t][iQs][iQp][1:t] # Store the "proportion" of mated individuals
        
            Remaining_Population[iQs,:,1:(t+1)] -= Become_Mated[t,iQs,:,1:(t+1)] # Mated individuals are removed from the population
            
        ## The remaining individuals (for which none of the previous events happened) stay in current state until next time step ##
        for iQs in range(len(Qs_classes)):
            for iQp in range(len(Qp_classes)) :
                if iQp == 0:
                    Distribution_Matrix[t+1][iQs][0][0] += Remaining_Population[iQs][0][0] # if single : stay single until next time step 
                else :
                    Distribution_Matrix[t+1][iQs][iQp][2:t+2] = Remaining_Population[iQs][iQp][1:(t+1)] # if in courtship : continue courtship with current partner until next time step (t_prime += 1) 

    TRUC.append(np.sum(Distribution_Matrix[t]) + np.sum(Mate[t])) #Somme des individus qui devrait faire 1 mais qui fait pas 1
###

## Plot pour voir le bug ##
TIME = [t for t in range(T_final)]
plt.plot(TIME,TRUC)
plt.show()
